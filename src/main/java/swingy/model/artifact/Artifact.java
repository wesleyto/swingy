// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Artifact.java                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wto <marvin@42.fr>                         +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/12/01 15:39:15 by wto               #+#    #+#             //
//   Updated: 2018/12/01 15:39:15 by wto              ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

package swingy.model.artifact;

public abstract class Artifact
{
	protected int level;
	protected int atkBuff;
	protected int defBuff;
	protected int spdBuff;

	public int getLevel()
	{
		return this.level;
	}

	public int getAtkBuff()
	{
		return this.atkBuff;
	}

	public int getDefBuff()
	{
		return this.defBuff;
	}

	public int getSpdBuff()
	{
		return this.spdBuff;
	}
}