// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Console.java                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wto <marvin@42.fr>                         +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/11/30 22:59:50 by wto               #+#    #+#             //
//   Updated: 2018/11/30 22:59:50 by wto              ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

package swingy.view;

import swingy.GameManager;

public class Console extends View
{

	public Console(GameManager gm)
	{
		super(gm);
	}

	public void update()
	{
		if (this.active)
		{
			System.out.println(this.attributes.get("title"));
		}
	}

	public void close()
	{
		if (this.active)
		{
			System.out.println("CLOSE");
		}
	}
}