// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   TextPanel.java                                     :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wto <marvin@42.fr>                         +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/12/02 19:39:23 by wto               #+#    #+#             //
//   Updated: 2018/12/02 19:39:26 by wto              ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

package swingy.view;

import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.awt.BorderLayout;

public class TextPanel extends JPanel
{
	private JTextArea textArea;

	public TextPanel()
	{
		textArea = new JTextArea();
		setLayout(new BorderLayout());
		add(new JScrollPane(textArea), BorderLayout.CENTER);
	}

	public void append(String s)
	{
		textArea.append(s);
	}
}