// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Attributes.java                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wto <marvin@42.fr>                         +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/11/30 23:30:38 by wto               #+#    #+#             //
//   Updated: 2018/11/30 23:30:38 by wto              ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

package swingy;

import swingy.exceptions.ExceptionHandler;
import java.util.Properties;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Attributes
{
	private static Attributes instance;
	private static Properties properties;

	private Attributes()
	{
		// load properties
		properties = new Properties();
		try
		{
			FileInputStream fin = new FileInputStream("gameSettings.properties");
			properties.load(fin);
			fin.close();
		}
		catch (FileNotFoundException e)
		{
			ExceptionHandler.exit(e);
		}
		catch (IOException e)
		{
			ExceptionHandler.exit(e);
		}
	}

	public static Attributes getInstance()
	{
		if (instance == null)
		{
			instance = new Attributes();
		}
		return instance;
	}

	public String get(String property)
	{
		return properties.getProperty(property);
	}

	public String getAsString(String property)
	{
		return get(property);
	}

	public int getAsInt(String property)
	{
		return Integer.valueOf(get(property));
	}

	public float getAsFloat(String property)
	{
		return Float.valueOf(get(property));
	}

	public double getAsDouble(String property)
	{
		return Double.valueOf(get(property));
	}
}