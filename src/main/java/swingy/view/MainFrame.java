// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   MainFrame.java                                     :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wto <marvin@42.fr>                         +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/11/30 23:24:57 by wto               #+#    #+#             //
//   Updated: 2018/11/30 23:24:57 by wto              ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

package swingy.view;

import swingy.view.TextPanel;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;

public class MainFrame extends JFrame
{
	private TextPanel textPanel;
	private JTextField textField;
	private JButton botBtn;

	public MainFrame(String title)
	{
		super(title);
		setLayout(new BorderLayout());
		textPanel = new TextPanel();
		textField = new JTextField();
		botBtn = new JButton("Don't Click Me");
		botBtn.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				textPanel.append(textField.getText());
				textPanel.append("\n");
			}
		});
		add(textPanel, BorderLayout.CENTER);
		add(botBtn, BorderLayout.SOUTH);
		add(textField, BorderLayout.NORTH);
		setSize(600, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}