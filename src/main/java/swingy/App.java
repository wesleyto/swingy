// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   App.java                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wto <marvin@42.fr>                         +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/11/30 23:05:22 by wto               #+#    #+#             //
//   Updated: 2018/11/30 23:05:22 by wto              ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

package swingy;

import swingy.exceptions.ExceptionHandler;
import swingy.exceptions.ModeException;
import swingy.model.hero.Hero;
import swingy.util.Validator;
import swingy.view.Mode;
import swingy.model.game.Map;

public class App 
{
	public static void main( String[] args )
	{
		try
		{
			if (args.length == 1)
			{
				Validator.validateMode(args[0]);
				Mode mode = args[0].equals("console") ? Mode.CONSOLE : Mode.GUI;
				GameManager gm = new GameManager(mode);
				// start game manager here, passing in the mode
			}
			else
			{
				System.out.println("usage: java -jar swingy.jar <gui|console>");
			}
		}
		catch (ModeException e)
		{
			ExceptionHandler.exit(e);
		}
		catch (NullPointerException e)
		{
			ExceptionHandler.exit(e);
		}
	}
}
