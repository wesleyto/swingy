// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Validator.java                                     :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wto <marvin@42.fr>                         +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/11/28 23:08:01 by wto               #+#    #+#             //
//   Updated: 2018/11/28 23:08:02 by wto              ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

package swingy.util;

import swingy.exceptions.ModeException;

public class Validator
{
	public static void validateMode(String mode) throws ModeException, NullPointerException
	{
		if (mode == null)
		{
			throw new NullPointerException();
		}
		if (!mode.equals("console") && !mode.equals("gui"))
		{
			throw new ModeException(mode);
		}
	}
}