// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ExceptionHandler.java                              :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wto <marvin@42.fr>                         +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/11/28 23:12:38 by wto               #+#    #+#             //
//   Updated: 2018/11/28 23:12:38 by wto              ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

package swingy.exceptions;

public class ExceptionHandler
{
	private ExceptionHandler()
	{
		// do nothing
	}

	public static void exit(Exception e)
	{
		System.out.println(e);
		System.exit(1);
	}
}