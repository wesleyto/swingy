// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Map.java                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wto <marvin@42.fr>                         +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/12/01 19:22:24 by wto               #+#    #+#             //
//   Updated: 2018/12/01 19:22:24 by wto              ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

package swingy.model.game;

import swingy.model.creature.Creature;
import swingy.model.villain.Villain;
import swingy.model.hero.Hero;
import java.util.Random;
import java.lang.StringBuffer;

enum Direction
{
	NORTH,
	SOUTH,
	WEST,
	EAST
}

public class Map
{
	private int size;
	private int hRow;
	private int hCol;
	private Random r;
	private Creature[][] map;

	public Map(int level)
	{
		this.size = (level - 1) * 5 + 10 - (level % 2);
		this.map = new Creature[this.size][this.size];
		this.r = new Random();
		int numVillains = (int)(this.r.nextGaussian() * (this.size) + (this.size * this.size / 4));
		int row, col;
		for (int i = 0; i < numVillains; i++)
		{
			row = this.r.nextInt(this.size);
			col = this.r.nextInt(this.size);
			this.map[row][col] = new Villain();
		}
		this.hRow = (int)this.size / 2;
		this.hCol = this.hRow;
		this.map[this.hRow][this.hCol] = null;
	}

	public void placeHero(Hero h)
	{
		this.map[this.hRow][this.hCol] = h;
	}

	public String toString()
	{
		Creature curr;
		StringBuffer s = new StringBuffer();
		for (int row = 0; row < this.size; row++)
		{
			for (int col = 0; col < this.size; col++)
			{
				curr = this.map[row][col];
				if (curr != null)
				{
					if (curr instanceof Hero)
					{
						s.append("H ");
					}
					else
					{
						s.append("V ");
					}
				}
				else
				{
					s.append(". ");
				}
			}
			s.append("\n");
		}
		return s.toString();
	}

	public void moveHero(Direction d)
	{
		Hero h = (Hero)this.map[this.hRow][this.hCol];
		this.map[this.hRow][this.hCol] = null;
		switch(d)
		{
			case NORTH:
				this.hRow--;
				break;
			case SOUTH:
				this.hRow++;
				break;
			case WEST:
				this.hCol--;
				break;
			case EAST:
				this.hCol++;
				break;
		}
		if (this.hRow < this.size && this.hRow >= 0 && this.hCol < this.size && this.hCol >= 0)
		{
			this.map[this.hRow][this.hCol] = h;
		}
	}

	public Villain getVillain()
	{
		return (Villain)this.map[this.hRow][this.hCol];
	}
}