// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Simulator.java                                     :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wto <marvin@42.fr>                         +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/12/01 17:07:58 by wto               #+#    #+#             //
//   Updated: 2018/12/01 17:08:21 by wto              ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

package swingy.model.game;

import swingy.model.hero.Hero;
import swingy.model.villain.Villain;
import java.util.Random;

public class Simulator
{
	private static Random r;
	private static Simulator instance;

	private Simulator()
	{
		r = new Random();
	}

	public static Simulator getInstance()
	{
		if (instance == null)
		{
			instance = new Simulator();
		}
		return (instance);
	}

	public static void simulateBattle(Hero h, Villain v)
	{
		if (h.getSpeed() == v.getSpeed())
		{
			if (r.nextInt(2) == 0)
			{
				h.attack(v);
			}
			while (h.isAlive() && v.isAlive())
			{
				v.attack(h);
				if (h.isAlive())
				{
					h.attack(v);
				}
			}
		}
		else
		{
			int turn = 1;
			while (h.isAlive() && v.isAlive())
			{
				boolean isHTurn = turn % v.getSpeed() == 0;
				boolean isVTurn = turn % h.getSpeed() == 0;
				if (isHTurn && isVTurn)
				{
					if (r.nextInt(2) == 0)
					{
						v.attack(h);
						if (h.isAlive())
						{
							h.attack(v);
						}
					}
					else
					{
						h.attack(v);
						if (v.isAlive())
						{
							v.attack(h);
						}
					}
				}
				else if (isVTurn)
				{
					v.attack(h);
				}
				else if (isHTurn)
				{
					h.attack(v);
				}
				turn++;
			}
		}
	}
}