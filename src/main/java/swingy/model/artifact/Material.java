// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Material.java                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wto <marvin@42.fr>                         +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/12/01 16:00:58 by wto               #+#    #+#             //
//   Updated: 2018/12/01 16:00:58 by wto              ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

package swingy.model.artifact;

public class Material
{
	private static final String[] materials = {
		" Wood ", " Stone ", " Bronze ", " Iron ", " Steel "
	};
}