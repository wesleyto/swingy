// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ModeException.java                                 :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wto <marvin@42.fr>                         +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/11/28 23:05:10 by wto               #+#    #+#             //
//   Updated: 2018/11/28 23:05:11 by wto              ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

package swingy.exceptions;

public class ModeException extends Exception
{
	public ModeException(String message)
	{
		super(String.format("Unrecognized mode: '%s'", message));
	}
}