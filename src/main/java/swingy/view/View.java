// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   View.java                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wto <marvin@42.fr>                         +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/11/30 23:01:06 by wto               #+#    #+#             //
//   Updated: 2018/11/30 23:01:06 by wto              ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

package swingy.view;

import swingy.Attributes;
import swingy.GameManager;

public abstract class View
{
	protected boolean active = false;
	protected GameManager gameManager;
	protected Attributes attributes;

	public View(GameManager gm)
	{
		this.gameManager = gm;
		this.attributes = Attributes.getInstance();
	}

	public void activate()
	{
		active = true;
	}

	public void deactivate()
	{
		active = false;
	}

	public abstract void update();
	public abstract void close();
	// public abstract void getAction();
	// public void getInput();
	/*
	public void keyEventHandler()
	{
		https://stackoverflow.com/questions/4522090/how-can-i-detect-arrow-keys-in-java
	}
	*/
}