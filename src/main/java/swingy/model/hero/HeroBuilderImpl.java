// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   HeroBuilderImpl.java                               :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wto <marvin@42.fr>                         +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/11/29 00:26:36 by wto               #+#    #+#             //
//   Updated: 2018/11/29 00:26:36 by wto              ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

package swingy.model.hero;

public class HeroBuilderImpl implements HeroBuilder
{
	private Hero heroTmp;
	private HeroBuilderImpl instance;

	private HeroBuilderImpl()
	{
		heroTmp = new Hero();
	}

	public HeroBuilderImpl getInstance()
	{
		if (instance == null)
		{
			instance = new HeroBuilderImpl();
		}
		return instance;
	}

	@Override
	public Hero build()
	{
		Hero hero = new Hero();
		hero.setName(heroTmp.getName());
		hero.setHeroClass(heroTmp.getHeroClass());
		hero.setLevel(heroTmp.getLevel());
		hero.setXP(heroTmp.getXP());
		hero.setAttack(heroTmp.getAttack());
		hero.setDefense(heroTmp.getDefense());
		hero.setHP(heroTmp.getHP());
		return hero;
	}

	@Override
	public HeroBuilder setName(final String val)
	{
		heroTmp.setName(val);
		return this;
	}

	@Override
	public HeroBuilder setHeroClass(final HeroClass val)
	{
		heroTmp.setHeroClass(val);
		return this;
	}

	@Override
	public HeroBuilder setLevel(final int val)
	{
		heroTmp.setLevel(val);
		return this;
	}

	@Override
	public HeroBuilder setXP(final int val)
	{
		heroTmp.setXP(val);
		return this;
	}

	@Override
	public HeroBuilder setSpeed(final int val)
	{
		heroTmp.setSpeed(val);
		return this;
	}

	@Override
	public HeroBuilder setAttack(final float val)
	{
		heroTmp.setAttack(val);
		return this;
	}

	@Override
	public HeroBuilder setDefense(final float val)
	{
		heroTmp.setDefense(val);
		return this;
	}

	@Override
	public HeroBuilder setHP(final float val)
	{
		heroTmp.setHP(val);
		return this;
	}

}