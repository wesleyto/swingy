// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Creature.java                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wto <marvin@42.fr>                         +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/12/01 17:21:22 by wto               #+#    #+#             //
//   Updated: 2018/12/01 17:21:23 by wto              ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

package swingy.model.creature;

public abstract class Creature
{
	protected String name;
	protected int level;
	protected int xp;
	protected int spd;
	protected float atk;
	protected float def;
	protected float hp;

	public void attack(Creature other)
	{
		other.setHP(other.getHP() - this.getAttack());
	}

	public boolean isAlive()
	{
		return this.hp > 0;
	}

	public String getName()
	{
		return this.name;
	}

	public int getLevel()
	{
		return this.level;
	}

	public int getXP()
	{
		return this.xp;
	}

	public int getSpeed()
	{
		return this.spd;
	}

	public float getAttack()
	{
		return this.atk;
	}

	public float getDefense()
	{
		return this.def;
	}

	public float getHP()
	{
		return this.hp;
	}

	public void setName(final String val)
	{
		this.name = val;
	}

	public void setLevel(final int val)
	{
		this.level = val;
	}

	public void setXP(final int val)
	{
		this.xp = val;
	}

	public void setSpeed(final int val)
	{
		this.spd = val;
	}

	public void setAttack(final float val)
	{
		this.atk = val;
	}

	public void setDefense(final float val)
	{
		this.def = val;
	}

	public void setHP(final float val)
	{
		this.hp = val;
	}
}