// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   HeroBuilder.java                                   :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wto <marvin@42.fr>                         +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/11/29 00:23:46 by wto               #+#    #+#             //
//   Updated: 2018/11/29 00:23:47 by wto              ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

package swingy.model.hero;

import swingy.model.artifact.Helmet;
import swingy.model.artifact.Armor;
import swingy.model.artifact.Weapon;

public interface HeroBuilder
{
	Hero build();
	HeroBuilder setName(final String val);
	HeroBuilder setHeroClass(final HeroClass val);
	HeroBuilder setLevel(final int val);
	HeroBuilder setXP(final int val);
	HeroBuilder setSpeed(final int val);
	HeroBuilder setAttack(final float val);
	HeroBuilder setDefense(final float val);
	HeroBuilder setHP(final float val);
}