#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: wto <marvin@42.fr>                         +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/11/28 22:47:22 by wto               #+#    #+#              #
#    Updated: 2018/11/28 22:47:22 by wto              ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME		=	swingy.jar
OUTDIR		=	./target

RM			=	/bin/rm
JAVA		=	java
COMPILER	=	mvn

FLAGS		=	-jar
BLDFLAGS	=	clean package
RMFLAGS		=	-rf

# ifndef VERBOSE
# .SILENT:
# endif

all:
	$(COMPILER) $(BLDFLAGS)

clean:
	$(RM) $(RMFLAGS) $(OUTDIR)

fclean: clean
	$(RM) $(RMFLAGS) $(NAME)

run:
	$(JAVA) $(FLAGS) $(NAME) $(GUI)

re: fclean all
