// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Action.java                                        :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wto <marvin@42.fr>                         +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/12/05 22:19:10 by wto               #+#    #+#             //
//   Updated: 2018/12/05 22:19:10 by wto              ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

package swingy.model.game;

public enum Action
{
	NONE,
	MOVE_U,
	MOVE_D,
	MOVE_L,
	MOVE_R,
	FIGHT,
	RUN,
	EQUIP,
	DROP
};