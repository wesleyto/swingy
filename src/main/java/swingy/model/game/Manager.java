// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Manager.java                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wto <marvin@42.fr>                         +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/12/01 19:37:23 by wto               #+#    #+#             //
//   Updated: 2018/12/01 19:37:23 by wto              ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

package swingy.model.game;

import swingy.model.hero.Hero;

public class Manager
{
	private Map map;
	private Simulator simulator;
	private Hero hero;

	public Manager(Hero h)
	{
		this.hero = h;
		this.map = new Map(h.getLevel());
		this.simulator = Simulator.getInstance();
	}

	public void run()
	{
		/*
		while true
		{
			while hero is in bounds
			{
				get input
				see if there is a villain in movement direction
				if movement causes collision
					ask if user wants to fight or run
					if fight:
						simulate fight
						if win:
							null the villain
							update hero level
								if artifact dropped:
									replace or drop
							move hero
						else:
							game over
					if run:
						if random(0, 1) == 0:
							simulate fight
							if win:
								null the villain
								update hero level
								if artifact dropped:
									replace or drop
								move hero
							else:
							game over
						else:
							do not move

			}
			map = new Map(h.getLevel());
			map.placeHero(h);
		}
		*/
	}
}