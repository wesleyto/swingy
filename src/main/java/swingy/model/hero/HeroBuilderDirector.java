// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   HeroBuilderDirector.java                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wto <marvin@42.fr>                         +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/11/29 00:26:43 by wto               #+#    #+#             //
//   Updated: 2018/11/29 00:26:43 by wto              ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

package swingy.model.hero;

public class HeroBuilderDirector
{
	private HeroBuilder builder;

	public HeroBuilderDirector(final HeroBuilder builder)
	{
		this.builder = builder;
	}

	public Hero construct()
	{
		return builder.setName("asdf").build(); // fill in the rest of the attributes
	}
}