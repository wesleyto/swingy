// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   GameManager.java                                   :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wto <marvin@42.fr>                         +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/12/04 19:55:23 by wto               #+#    #+#             //
//   Updated: 2018/12/04 19:55:24 by wto              ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

package swingy;

import swingy.view.Console;
import swingy.view.Mode;
import swingy.view.View;
import swingy.view.Gui;

public class GameManager
{
	private View console;
	private View gui;
	//private Simulator simulator;
	
	public GameManager(Mode mode)
	{
		console = new Console(this);
		gui = new Gui(this);
		if (mode == Mode.CONSOLE)
		{
			console.activate();
		}
		else
		{
			gui.activate();
		}
		while (true)
		{
			//get input
			/*
			console.getInput();
			gui.getInput();

			action = console.getAction();
			if (!action)
			{
				action = gui.getAction();
			}

			game.update(action);

			console.update(game.getState());
			gui.update(game.getState());
			*/
		}
	}
}