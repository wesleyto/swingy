// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Hero.java                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wto <marvin@42.fr>                         +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/11/28 23:46:08 by wto               #+#    #+#             //
//   Updated: 2018/11/28 23:46:09 by wto              ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

package swingy.model.hero;

import swingy.model.creature.Creature;
import swingy.model.artifact.Helmet;
import swingy.model.artifact.Armor;
import swingy.model.artifact.Weapon;

public class Hero extends Creature
{
	private HeroClass cls;
	private Helmet helmet;
	private Armor armor;
	private Weapon weapon;

	public HeroClass getHeroClass()
	{
		return this.cls;
	}

	public Helmet getHelmet()
	{
		return this.helmet;
	}

	public Armor getArmor()
	{
		return this.armor;
	}

	public Weapon getWeapon()
	{
		return this.weapon;
	}

	public void setHeroClass(final HeroClass val)
	{
		this.cls = val;
	}

	public void setHelmet(final Helmet val)
	{
		this.helmet = val;
	}

	public void setArmor(final Armor val)
	{
		this.armor = val;
	}

	public void setWeapon(final Weapon val)
	{
		this.weapon = val;
	}

	@Override
	public int getSpeed()
	{
		return this.spd + this.helmet.getSpdBuff()
			+ this.armor.getSpdBuff()
			+ this.weapon.getSpdBuff();
	}

	@Override
	public float getAttack()
	{
		return this.atk + this.helmet.getAtkBuff()
			+ this.armor.getAtkBuff()
			+ this.weapon.getAtkBuff();
	}

	@Override
	public float getDefense()
	{
		return this.def + this.helmet.getDefBuff()
			+ this.armor.getDefBuff()
			+ this.weapon.getDefBuff();
	}

	public void updateLevel()
	{
		// level * 1000 + (level − 1)^2 * 450
		double a = 450.0;
		double b = 100.0;
		double c = 450 - this.xp;
		double discriminant = b * b - 4 * a * c;
		double sqrt_discriminant = Math.sqrt(discriminant);
		double z0 = (-b + sqrt_discriminant) / (2 * a);
		double z1 = (-b - sqrt_discriminant) / (2 * a);
		this.level = (int)Math.max(z0, z1);
	}
}