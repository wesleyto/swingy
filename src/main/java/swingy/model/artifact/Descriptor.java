// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Descriptor.java                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wto <marvin@42.fr>                         +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/12/01 16:01:08 by wto               #+#    #+#             //
//   Updated: 2018/12/01 16:01:09 by wto              ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

package swingy.model.artifact;

public class Descriptor
{
	private static final String[] descriptors = {
		"Poor", "Mediocre", "Average", "Good", "Amazing", "Magnificent"
	};

}