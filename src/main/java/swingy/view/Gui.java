// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Gui.java                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wto <marvin@42.fr>                         +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/11/30 22:59:55 by wto               #+#    #+#             //
//   Updated: 2018/11/30 22:59:55 by wto              ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

package swingy.view;

import swingy.GameManager;
import javax.swing.JFrame;
import java.awt.event.WindowEvent;

public class Gui extends View
{
	private JFrame frame;

	public Gui(GameManager gm)
	{
		super(gm);
		frame = new MainFrame(this.attributes.get("title"));
	}

	@Override
	public void activate()
	{
		active = true;
		frame.setVisible(active);
	}

	@Override
	public void deactivate()
	{
		active = false;
		frame.setVisible(active);
	}

	public void update()
	{
		if (this.active)
		{
			;
		}
	}

	public void close()
	{
		if (this.active)
		{
			frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
		}
	}
}