package swingy.util;

import swingy.exceptions.ModeException;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

public class ValidatorTest 
{
	@Test
	public void emptyMode()
	{
		try
		{
			Validator.validateMode("");
			assertTrue( false );
		}
		catch (NullPointerException e)
		{
			assertTrue( false );
		}
		catch (ModeException e)
		{
			assertTrue( true );
		}
	}
	
	@Test
	public void nullMode()
	{
		try
		{
			Validator.validateMode(null);
			assertTrue( false );
		}
		catch (NullPointerException e)
		{
			assertTrue( true );
		}
		catch (ModeException e)
		{
			assertTrue( false );
		}
	}
	
	@Test
	public void singleCharMode()
	{
		try
		{
			Validator.validateMode("a");
			assertTrue( false );
		}
		catch (NullPointerException e)
		{
			assertTrue( false );
		}
		catch (ModeException e)
		{
			assertTrue( true );
		}
	}
	
	@Test
	public void unrecognizedMode()
	{
		try
		{
			Validator.validateMode("asdf");
			assertTrue( false );
		}
		catch (NullPointerException e)
		{
			assertTrue( false );
		}
		catch (ModeException e)
		{
			assertTrue( true );
		}
	}
	
	@Test
	public void consoleMode()
	{
		try
		{
			Validator.validateMode("console");
			assertTrue( true );
		}
		catch (NullPointerException e)
		{
			assertTrue( false );
		}
		catch (ModeException e)
		{
			assertTrue( false );
		}
	}
	
	@Test
	public void guiMode()
	{
		try
		{
			Validator.validateMode("gui");
			assertTrue( true );
		}
		catch (NullPointerException e)
		{
			assertTrue( false );
		}
		catch (ModeException e)
		{
			assertTrue( false );
		}
	}
}
